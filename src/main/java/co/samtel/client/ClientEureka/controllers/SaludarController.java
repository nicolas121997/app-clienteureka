package co.samtel.client.ClientEureka.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/saludar")
public class SaludarController {
	
	@GetMapping("/hola")
	public String saludar() {
		return "hola, amigo";
	}
}
